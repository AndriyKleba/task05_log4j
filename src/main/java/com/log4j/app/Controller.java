package com.log4j.app;

public class Controller {
    private Student model;
    private ViewStudent view;

    public Controller() {
    }

    public Controller(Student model, ViewStudent view){
        this.model = model;
        this.view = view;
    }

    public void setStudentName(String name){
        model.setName(name);
    }

    public String getStudentName(){
        return model.getName();
    }

    public void setStudentId(String id){
        model.setId(id);
    }

    public String getStudentId(){
        return model.getId();
    }

    public void updateView(){
        view.showStudentDetail(model.getName(), model.getId());
    }
}
