package com.log4j.app;

import org.apache.logging.log4j.*;

public class App {

    private static Logger logger = LogManager.getLogger(App.class.getName());

    public static void main(String[] args) {

        logger.trace("This is a trace message");
        logger.debug("This is a debug message");
        logger.info("This is a info message");
        logger.warn("This is a warm message");
        logger.error("This is a error message");
        logger.fatal("This is a fatal message");

//        Student model = getStudentDatabase();
//
//        ViewStudent view = new ViewStudent();
//
//        Controller controller = new Controller(model, view);
//
//        controller.updateView();
//
//        controller.setStudentName("Андрій");
//
//        controller.updateView();
//
//
//    }
//
//    private static Student getStudentDatabase() {
//        Student student = new Student();
//        student.setName("Сергій");
//        student.setId("456431343");
//        return student;
    }
}
